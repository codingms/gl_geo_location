## Geo-Location Change-Log


## 2017-09-28  Release of version 1.6.1

## 2017-04-03  Release of version 1.2.1

### 2017-04-03  Thomas Deuling  <typo3@coding.ms>

*	[TASK] License key



## 2017-04-03  Release of version 1.2.0

### 2017-04-03  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Add formatted address



## 2017-03-21  Release of version 1.1.1

### 2017-03-21  Thomas Deuling  <typo3@coding.ms>

*	[BUGFIX] Fixing countAll on GeoLocationRepository



## 2017-03-09  Release of version 1.1.0

### 2017-03-09  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Geo-Location records are now stored in different pages/container, so that they can be grouped.

### 2017-03-07  Thomas Deuling  <typo3@coding.ms>

*	[FEATURE] Adding a findAllByGeoCoordinatesInDistance Repository method, for getting Geo-Locations



## 2017-03-05  Release of version 1.0.0