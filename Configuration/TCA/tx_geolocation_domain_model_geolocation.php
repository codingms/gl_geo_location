<?php
return array(
    'ctrl' => array(
        'title' => 'LLL:EXT:geo_location/Resources/Private/Language/locallang_db.xlf:tx_geolocation_domain_model_geolocation',
        'label' => 'address',
        'label_alt' => 'formatted_address',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'rootLevel' => -1,

        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'searchFields' => 'address,latitude,longitude,',
        'iconfile' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('geo_location') . 'Resources/Public/Icons/iconmonstr-location-21.svg'
    ),
    'interface' => array(
        'showRecordFieldList' => 'address, formatted_address, latitude, longitude',
    ),
    'types' => array(
        '1' => array('showitem' => 'address, formatted_address, latitude, longitude'),
    ),
    'palettes' => array(
        '1' => array('showitem' => ''),
    ),
    'columns' => array(

        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'starttime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),
        'endtime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
            ),
        ),

        'address' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:geo_location/Resources/Private/Language/locallang_db.xlf:tx_geolocation_domain_model_geolocation.address',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
        'formatted_address' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:geo_location/Resources/Private/Language/locallang_db.xlf:tx_geolocation_domain_model_geolocation.formatted_address',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ),
        ),
        'latitude' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:geo_location/Resources/Private/Language/locallang_db.xlf:tx_geolocation_domain_model_geolocation.latitude',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'double2'
            )
        ),
        'longitude' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:geo_location/Resources/Private/Language/locallang_db.xlf:tx_geolocation_domain_model_geolocation.longitude',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'eval' => 'double2'
            )
        ),

    ),
);